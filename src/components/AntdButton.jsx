import { Button } from 'antd'

export const AntdButton = ({ block, danger, disabled, ghost, href, loading, shape, size, target, type, customSize, radius, content, backgroundColor, icon, ...props }) => {
  const sizes = {
    'xs': '24px',
    'sm': '32px',
    'md': '40px',
    'lg': '48px',
    'xl': '56px'
  }

  const borderRadius = {
    'simple': '0',
    'round': '8px',
    'circle': '999px',
  }

  return (
    <Button
      block={block}
      danger={danger}
      disabled={disabled}
      ghost={ghost}
      href={href}
      loading={loading}
      shape={shape}
      size={size}
      target={target}
      type={type}
      icon={icon}
      style={{ height: sizes[customSize], borderRadius: borderRadius[radius], backgroundColor: backgroundColor }}
      {...props}
    >
      {content}
    </Button>
  );
};

