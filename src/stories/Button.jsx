import React from 'react';
import PropTypes from 'prop-types';
import './button.css';
import { AntdButton } from '../components/AntdButton'
import { FireOutlined } from '@ant-design/icons'
import {createUseStyles} from 'react-jss'

const useStyles = createUseStyles({
  container: {
    display: 'flex',
    alignItems: 'center',
    height: '100%',
    color: '#5850EC'
  },
  spanBase: {
    fontSize: '16px',
    fontWeight: '500'
  },
  spanSm: {
    fontSize: '14px',
    fontWeight: '500',
    color: '#8DA2FB'
  },
  textWrapper: {
    padding: '10px 10px'
  }
})

export const Button = ({ block, danger, disabled, ghost, href, loading, shape, size, target, type, customSize, radius, tailwind, backgroundColor, icon }) => {
  const contentTailwind = (
    <div className='flex items-center h-full text-white'>
      <FireOutlined className='pr-2' />
        <span className='text-base font-500'>button&nbsp;</span>
        <span className='text-sm font-500 text-[#8DA2FB]'>/&nbsp;02</span>
      <FireOutlined className='pl-2' />
    </div>
  )
  
  const classes= useStyles()

  const contentCssInJs = (
    <div className={classes.container}>
      <FireOutlined />
        <div className={classes.textWrapper}>
          <span className={classes.spanBase}>button&nbsp;</span>
          <span className={classes.spanSm}>/&nbsp;02</span>
        </div>
      <FireOutlined />
    </div>
  )

  return (
    <AntdButton
      block={block}
      danger={danger}
      disabled={disabled}
      ghost={ghost}
      href={href}
      loading={loading}
      shape={shape}
      size={size}
      target={target}
      type={type}
      customSize={customSize}
      radius={radius}
      icon={icon}
      content={tailwind ? contentTailwind : contentCssInJs}
      backgroundColor={backgroundColor}
    />
  );
};

Button.propTypes = {

  block: PropTypes.bool,
  danger: PropTypes.bool,
  disabled: PropTypes.bool,
  ghost: PropTypes.bool,

  href: PropTypes.string,

  loading: PropTypes.bool,

  shape: PropTypes.oneOf(['default', 'circle', 'round']),

  size: PropTypes.oneOf(['small', 'middle', 'large']),

  target: PropTypes.string,

  type: PropTypes.oneOf(['primary', 'dashed', 'link', 'text', 'default']),

  customSize: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),

  radius: PropTypes.oneOf(['simple', 'round', 'circle']),

  onClick: PropTypes.func,
};

Button.defaultProps = {
  block: false,
  danger: false,
  disabled: false,
  ghost: false,
  href: null, 
  loading: false,
  shape: 'default',
  size: 'middle',
  target: null,
  type: 'default',
  onClick: undefined,
};
